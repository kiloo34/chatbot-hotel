from django.contrib import admin
from django.urls import path, include

from chatbot import views

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', views.index, name='landing'),
    
    # auth
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('guest/', views.guest, name='guest'),
    path('logout/', views.logout, name='logout'),

    path('dashboard/', views.dashboard, name='dashboard'),
    path('kamar/', views.kamar, name='kamar'),
    path('profile/', views.profile, name='profile'),
    
    path('chat/', views.chat, name='chat'), #return to view
    path('ajax/kirimPesan1', views.kirimPesan, name='kirimPesan'),
    path('ajax/templateChat/', views.templateChat, name='templateChat'),
    path('ajax/getResponse/', views.getResponse, name='getResponSystem'),

    path('booking/', views.orderKamar, name='booking'),
    path('ajax/get-harga-kamar/<int:kamar>/', views.getHargaKamar, name='hargaKamar'),
    # path("select2/", include(/"django_select2.urls")),
]
