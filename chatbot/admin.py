from django.contrib import admin
from chatbot.models import KategoriKamar, Kamar, KategoriPesan, Respon, Pesan, Fasilitas, Booking, DetailBooking

# Register your models here.
admin.site.register(KategoriKamar)
admin.site.register(Kamar)
admin.site.register(KategoriPesan)
admin.site.register(Respon)
# admin.site.register(Pesan)
admin.site.register(Fasilitas)
admin.site.register(Booking)
admin.site.register(DetailBooking)