import re

from django.core import serializers
from pandas import value_counts

from chatbot.models import Respon, KategoriPesan, Kamar, Fasilitas
from django.db.models import Avg, Max, Min
from chatbot.preprocessing import *
from chatbot.levenshtein import *

from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

GREETING_INPUTS = ("hallo","halo","hai","selamat","pagi","siang","sore","hello","helo","assalamualaikum")
PENUTUP_INPUTS = ("terima","kasih","terimakasih","thank","wallaikumsalam")

def getResponData():
    data = Respon.objects.all()
    return data

def getResponGreeting():
    data = Respon.objects.filter(kategori=1).values('text').first()
    return data['text']

def getTokenGreeting():
    return Respon.objects.filter(kategori=1).values('token','kategori').first()

def getResponPenutup():
    data = Respon.objects.filter(kategori=5).values('text').first()
    return data['text']

def getTokenPenutup():
    return Respon.objects.filter(kategori=5).values('token','kategori').first()

def getKategoriPesan():
    data = KategoriPesan.objects.all()
    return data

def getKategoriPesanId(id):
    data = KategoriPesan.objects.filter(id=id).values('nama').first()
    return data

def getNamaKamar():
    data = []
    kamar = Kamar.objects.all()
    for d in kamar:
        dk = d.nama
        data.append(dk.lower())
    return data

def getHargaKamar(nama):
    return Kamar.objects.filter(nama=nama).values('tarif').first()

def getMinHargaKamar():
    return Kamar.objects.all().values('tarif').aggregate(Min('tarif'))

def getMaxHargaKamar():
    return Kamar.objects.all().values('tarif').aggregate(Max('tarif'))

def getFasilitasKamar(nama):
    return Kamar.objects.filter(nama=nama).values('fasilitas__fasilitas').first()

def getBot(kategori, kamar):
    if kategori == 'fasilitas kamar':
        if kamar != '':
            text = Respon.objects.filter(kategori__nama__contains=kategori).filter(kamar__nama__contains=kamar).first()
            fas = getFasilitasKamar(kamar.upper())
            respon = str(text) + ' ' + fas['fasilitas__fasilitas']
        else:
            text = Respon.objects.filter(kategori__nama__contains=kategori).exclude(kamar__isnull=False).first()
            respon = str(text)

    elif kategori == 'harga kamar':
        if kamar != '':
            text = Respon.objects.filter(kategori__nama__contains=kategori).filter(kamar__nama__contains=kamar).first()
            tarif = getHargaKamar(kamar.upper())

            respon = str(text)+' '+tarif['tarif']
        else:
            text = Respon.objects.filter(kategori__nama__contains=kategori).exclude(kamar__isnull=False).first()
            
            tarifMin = getMinHargaKamar()
            tarifMax = getMaxHargaKamar()

            respon = str(text)+' '+tarifMin['tarif__min']+' - '+tarifMax['tarif__max']
    elif kategori == 'pesan kamar' or kategori == 'sewa kamar':
        text = Respon.objects.filter(kategori__nama__contains=kategori).exclude(kamar__isnull=False).first()
        # link = "{% url 'booking' %}"
        link = '<a href="https://wa.me/62895358120427" target="_blank"> Booking Page </a>'
        respon = str(text)+' '+link
    else:
        respon = unknown()
    return respon

def subtMessage(message, condition):
    res = []
    for m in message:
        if m in condition:
            res.append(m)
    return res

def checkMessage(message, condition):
    res = []
    for m in message:
        if m in condition:
            res.append('true')
    return res

def greeting(message):
    for m in message:
        if m in GREETING_INPUTS:
            res = Respon.objects.filter(kategori=1).first()
            return res.text

def penutup(message):
    for m in message:
        # print(m)
        if m in PENUTUP_INPUTS:
            res = Respon.objects.filter(kategori=5).first()
            return res.text

def check_all_messages(message):
    result_levenshtein_list = {}
    
    def process(kategori, st, ss):
        # print(st)
        # print(ss)
        nonlocal result_levenshtein_list
        result_levenshtein_list[kategori] = levenshtein(st.replace(" ", ""), ss.replace(" ", ""))

    respon = getResponData()
    data = getKategoriPesan()

    if greeting(message):
        token = getTokenGreeting()
        append_message = subtMessage(message, token.get('token'))
        kategori = getKategoriPesanId(token.get('kategori'))

        getPercent(arrToStringWithoutSpace(message), arrToStringWithoutSpace(append_message))
        process(kategori['nama'], arrToString(message), arrToString(append_message))

        kamar = getNamaKamar()
        res_kamar = subtMessage(message, kamar)
        best_match = max(result_levenshtein_list, key=result_levenshtein_list.get)
        respon = unknown() if result_levenshtein_list[best_match] < 0.5 else getResponGreeting()
    elif penutup(message):
        token = getTokenPenutup()
        append_message = subtMessage(message, token.get('token'))
        kategori = getKategoriPesanId(token.get('kategori'))

        print(token, append_message, kategori)

        getPercent(arrToStringWithoutSpace(message), arrToStringWithoutSpace(append_message))
        process(kategori['nama'], arrToString(message), arrToString(append_message))

        kamar = getNamaKamar()
        res_kamar = subtMessage(message, kamar)
        best_match = max(result_levenshtein_list, key=result_levenshtein_list.get)
        respon = unknown() if result_levenshtein_list[best_match] < 0.5 else getResponPenutup()
    else:
        for r in data:
            if r.list == None:
                token = Preprocessing(r.nama).run()
            else:
                token = r.list
                
            append_message = subtMessage(message, token)
            if checkMessage(message, ['kamar']):
                if checkMessage(token, ['pesan', 'sewa']) and checkMessage(message, ['pesan', 'sewa']):
                    # print('masuk if if')
                    token = subtMessage(token, append_message)
                elif checkMessage(token, ['fasilitas']) and checkMessage(message, ['fasilitas']):
                    # print('masuk if elif')
                    token = subtMessage(token, append_message)
            else:
                if checkMessage(token, ['pesan', 'sewa']) and checkMessage(message, ['pesan', 'sewa']):
                    # print('masuk if if')
                    token = subtMessage(token, append_message)
                elif checkMessage(token, ['fasilitas']) and checkMessage(message, ['fasilitas']):
                    # print('masuk if elif')
                    token = subtMessage(token, append_message)
            
            # qToken = Preprocessing(message).run()
            if set(append_message) == set('kamar'):
                print('masuk if kedua')
            elif append_message == []:
                append_message = message
            else:
                append_message = message
            # print(append_message, token)
            # # print(arrToString(message), arrToString(token))
            
            getPercent(arrToStringWithoutSpace(append_message), arrToStringWithoutSpace(token))

            # print(token);
            # print(append_message);
            # print(append_message == []);
            # print(set(append_message) == set('kamar'));

            process(r.nama, arrToString(append_message), arrToString(token))
        
        kamar = getNamaKamar()
        res_kamar = subtMessage(message, kamar)

        best_match = max(result_levenshtein_list, key=result_levenshtein_list.get)
        # print ('\n')
        # print(best_match)
        # print(result_levenshtein_list)
        # print(result_levenshtein_list[best_match])
        respon = unknown() if result_levenshtein_list[best_match] < 0.5 else getBot(best_match.lower(), arrToString(res_kamar))
    return respon;

def arrToString(array):
    return ' '.join(array)

def arrToStringWithoutSpace(array):
    return ''.join(array)

def unknown():
    text = 'respon tidak ditemukan, silakan hubungi agen Customer Service  '
    link = '<a href="https://wa.me/62895358120427" target="_blank"> disini </a>'
    return str(text)+' '+link

def get_response(user_input):
    qToken = Preprocessing(user_input).run()
    # print("\n=======================qToken====================")
    # print(' '.join(qToken))
    response = check_all_messages(qToken)
    # print("\n=======================Response====================")
    # print(response)
    if response == 'tidak ditemukan':
        pesan = user_input.translate(str.maketrans('','', string.punctuation))
        unknown_res = '"'+pesan+'"'+' '+response
        return unknown_res
    else:
        return response