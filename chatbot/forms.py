from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from . import models
from django import forms
from django_select2 import forms as select2Form
from .widgets import DatePickerInput, TimePickerInput, DateTimePickerInput

class LoginUserForm(forms.ModelForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text'}))
    password = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'password'}))

    class Meta:
        model = User

        fields = [
            'username',
            'password'
        ]

class CreateUserForm(UserCreationForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'email'}))
    password1 = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control pwstrength', 'type': 'password'}))
    password2 = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'password'}))

    class Meta:
        model = User

        fields = [
            'username',
            'email',
            'password1',
            'password2'
        ]

class KamarWidget(select2Form.ModelSelect2Widget):
    search_fields = [
        "kamar__nama__icontains",
    ]

class kategoriForm(forms.ModelForm):
    class Meta:
        model=models.KategoriKamar
        fields=('nama',)
        labels={
            'nama': 'Kategori Kamar'
        }

    def __init__(self, *args):
        super(kategoriForm, self).__init__(*args)

class bookingForm(forms.ModelForm):
    # tanggal = forms.DateField(widget=DatePickerInput)
    # kamar =
    # kategori = forms.ModelChoiceField(queryset=models.Kamar.objects.filter(kategori=kamar), empty_label='Pilih Kategori Kamar')
    class Meta:
        model=models.Booking
        fields=('tanggal', 'total')
        widgets={
            'tanggal': DatePickerInput,
        }
        labels={
            'tanggal': 'Tanggal Booking',
            'total': 'Total Boking'
        }

    def __init__(self, *args, **kwargs):
        super(bookingForm, self).__init__(*args, **kwargs)
        self.fields['total'].widget.attrs['readonly'] = True
        self.fields['total'].queryset = models.Kamar.objects.none()

class DetailBookingForm(forms.ModelForm):
    fasilitas = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text', 'readonly':'readonly'}))
    class Meta:
        model=models.DetailBooking
        fields=('kamar',)

    def __init__(self, *args, **kwargs):
        super(DetailBookingForm, self).__init__(*args, **kwargs)
        self.fields['kamar'].empty_label = "Pilih Kamar"

class KamarForm(forms.ModelForm):
    class Meta:
        model=models.Kamar
        fields=('nama', 'tarif', 'fasilitas')

    def __init__(self, *args):
        super(KamarForm, self).__init__(*args)
        
class KategoriKamarForm(forms.ModelForm):
    class Meta:
        model=models.KategoriKamar
        fields=('nama',)

    def __init__(self, *args):
        super(KategoriKamarForm, self).__init__(*args)

class ProfileForm(forms.ModelForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text'}))
    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text'}))
    last_name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'text'}))
    email = forms.CharField(
        widget=forms.EmailInput(attrs={'class': 'form-control', 'type': 'email'}))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    class Meta:
        model=User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password'
        ]

        # def __init__(self, *args, **kwargs):
            # super(ProfileForm, self).__init__(*args, **kwargs)
            # self.fields['password'].empty_label = "Pilih Kamar"
