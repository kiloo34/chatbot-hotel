from cgitb import text
import numpy as np
from chatbot.preprocessing import *

def levenshtein(str1, str2):
    m = len(str1)
    n = len(str2)

    lensum = float(m + n)
    cost = []
    
    for i in range(m+1):
        cost.append([i])

    del cost[0][0]
    for j in range(n+1):
        cost[0].append(j)

    for j in range(1,n+1):
        for i in range(1,m+1):
            if str1[i-1] == str2[j-1]:
                cost[i].insert(j,cost[i-1][j-1])
            else:
                minimum = min(cost[i-1][j]+1, cost[i][j-1]+1, cost[i-1][j-1]+2)
                cost[i].insert(j, minimum)

    ldist = cost[-1][-1]
    ratio = (lensum - ldist)/lensum
    # getPercent(str1, str2)
    return ratio

def getPercent(seq1, seq2):
    print(seq1, seq2)
    size_x = len(seq1) + 1
    size_y = len(seq2) + 1
    matrix = np.zeros ((size_x, size_y))
    percent = 0
    val = []

    for x in range(size_x):
        matrix [x, 0] = x
    for y in range(size_y):
        matrix [0, y] = y

    for x in range(1, size_x):
        for y in range(1, size_y):
            if seq1[x-1] == seq2[y-1]:
                matrix [x,y] = min(
                    matrix[x-1, y] + 1,
                    matrix[x-1, y-1],
                    matrix[x, y-1] + 1
                )
            else:
                matrix [x,y] = min(
                    matrix[x-1,y] + 1,
                    matrix[x-1,y-1] + 1,
                    matrix[x,y-1] + 1
                )

            if matrix[x, y] == 0:
                if x == y: 
                    val.append({abs(matrix[x, y] * (1/size_x))})

    # if len(val) != 0:
        # if len(seq1) == len(val):
            # percent = len(seq1) / len(val) * 100
            # percent = (1-(matrix[x-1,y-1]/max(size_x, size_y)))*100
    percent = (1-(matrix[x-1,y-1]/(max(size_x, size_y)-1)))*100
    # print()
    
    print(matrix)
    print('final : ',percent)
    return percent;