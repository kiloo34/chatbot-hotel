from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class KategoriKamar(models.Model):
    nama = models.CharField(max_length=50)

    class Meta:
        ordering = ['nama']

    def __str__(self) -> str:
        return self.nama

class Fasilitas(models.Model):
    fasilitas = models.TextField()

    class Meta:
        ordering = ['fasilitas']

    def __str__(self) -> str:
        return self.fasilitas
class Kamar(models.Model):
    kategori = models.ForeignKey(KategoriKamar, on_delete=models.CASCADE, null=True)
    nama = models.CharField(max_length=500)
    fasilitas = models.ForeignKey(Fasilitas, on_delete=models.CASCADE, null=True)
    tarif = models.CharField(max_length=50)

    class Meta:
        ordering = ['nama']

    def __str__(self):
        return str(self.nama)

class KategoriPesan(models.Model):
    nama = models.CharField(max_length=50)
    list = models.JSONField(null=True, blank=True)

    class Meta:
        ordering = ['nama']

    def __str__(self) -> str:
        return self.nama

class Respon(models.Model):
    text = models.TextField()
    kategori = models.ForeignKey(KategoriPesan, on_delete=models.CASCADE, null=True)
    kamar = models.ForeignKey(Kamar, on_delete=models.CASCADE, null=True, blank=True)
    token = models.CharField(max_length=500, blank=True)

    class Meta:
        ordering = ['kamar']

    def __str__(self) -> str:
        return self.text

class Pesan(models.Model):
    text = models.TextField()
    kategori = models.ForeignKey(KategoriPesan, on_delete=models.CASCADE, null=True)
    token = models.JSONField(null=True, blank=True)

    class Meta:
        ordering = ['text']

    def __str__(self) -> str:
        return self.text

class Booking(models.Model):
    total = models.IntegerField()
    tanggal = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['tanggal']

    def __str__(self) -> str:
        return str(self.tanggal)

class DetailBooking(models.Model):
    booking = models.ForeignKey(Booking, on_delete=models.CASCADE, null=True)
    kamar = models.ForeignKey(Kamar, on_delete=models.CASCADE, null=True)

    class Meta:
        ordering = ['kamar']

    def __str__(self) -> str:
        return str(self.booking.created_at)